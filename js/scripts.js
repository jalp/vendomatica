$.validator.setDefaults({
    submitHandler: function() {
        alert("submitted!");
    }
});

$().ready(function() {
    // validate signup form on keyup and submit
    $("#bookForm").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            date: "required",
            monto: "required",
            proServices: "required",
            reclamo_queja: "required",
            celphone: {
                required: true,
                minlength: 9,
                //number: true
            },
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            autorize: "required"
        },
        messages: {
            firstname: "Por favor ingrese sus nombres",
            lastname: "Por favor ingrese su apellido",
            date: "Por favor ingrese la fecha",
            proServices: "Seleccione producto o servicio",
            monto: "Por favor ingrese el monto",
            reclamo_queja: "Ingrese reclamo o queja",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            celphone: {
                required: "Por favor ingrese su celular",
                minlength: "Ingrese mínimo 9 caracteres",
                //number: "Solo se permiten números"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Por favor ingrese su correo",
            autorize: "Por favor acepte las políticas de privacidad",
            topic: "Please select at least 2 topics"
        }
    });

    // propose username by combining first- and lastname
    $("#username").focus(function() {
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        if (firstname && lastname && !this.value) {
            this.value = firstname + "." + lastname;
        }
    });

    //code to hide topic selection, disable for demo
    var newsletter = $("#newsletter");
    // newsletter topics are optional, hide at first
    var inital = newsletter.is(":checked");
    var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
    var topicInputs = topics.find("input").attr("disabled", !inital);
    // show when newsletter is checked
    newsletter.click(function() {
        topics[this.checked ? "removeClass" : "addClass"]("gray");
        topicInputs.attr("disabled", !this.checked);
    });
});